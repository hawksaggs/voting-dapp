pragma solidity >=0.5.0 <0.7.0;
pragma experimental ABIEncoderV2;

contract Voting {

  constructor() public {
    owner = msg.sender;
  }

  address public owner;

  struct Candidate {
    uint votes;
    uint candidateId;
    string name;
    address candidateAddress;
  }
  Candidate[] public candidates;

  mapping(address => bool) voters;

  function vote(uint candidateId) public returns (bool) {
    require(!voters[msg.sender], 'Vote is already being cast');
    bool isVoteCast = false;
    for(uint i = 0; i<candidates.length; i++) {
      if (candidates[i].candidateId == candidateId) {
        voters[msg.sender] = true;
        candidates[i].votes += 1;
        isVoteCast = true;
      }
    }
    return isVoteCast;
  }

  modifier onlyOwner() {
    require(msg.sender == owner, 'Only owner can acccess');
    _;
  }

  function setCandidate(string memory name, address candidateAddress) public onlyOwner returns (Candidate memory candidate) {
    uint totalCandidates = candidates.length;
    Candidate memory candidateVote = Candidate({votes: 0, candidateId: totalCandidates + 1, name: name, candidateAddress: candidateAddress});
    candidates.push(candidateVote);
    return candidate;
  }

  function getCandidateVote(address candidateAddress) public onlyOwner view returns (uint){
    uint votes = 0;
    for(uint i = 0; i<candidates.length; i++) {
      if (candidates[i].candidateAddress == candidateAddress) {
        votes = candidates[i].votes;
      }
    }
    return votes;
  }
}